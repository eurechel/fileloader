(function () {
    var app = angular.module('fileLoader', []);

    app.factory('userService', function($http){
        var baseUrl = 'http://klamathraven.com/FileLoader/Server/Services/';
        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded'}};

        return {
            login: function(userName, password) {
                //var data = {userName: userName, password: password};
                var data = "userName=" + userName + "&password=" + password;
                return $http.post(baseUrl + 'Login.php', data, config);
            }
        }
    });

    app.controller('UserController', UserController);
    UserController.$inject = ['$window', 'userService'];
    function UserController($window, userService) {
        this.userName = '';
        this.password = '';

        this.postForm = function () {
            userService.login(this.userName, this.password).then(
                function (successResponse) {
                    if (successResponse.data) {
                        $window.location.href = '/upload.html';
                    }
                    else {
                        alert("Log in failed...\r\n" + successResponse.data);
                    }
                },
                function (errorResponse) {
                    alert("ERROR\r\n" + errorResponse.data);
                }
            );
        }
    }



    app.controller('UploadController', function(){  //TODO: Add upload service
        this.description = '';

        this.postForm = function() {
            alert("Still TODO!\r\n" + this.description);
        }

    });
})();
