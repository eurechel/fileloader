<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 10/14/2017
 * Time: 5:17 PM
 */
    session_start();

    if(session_destroy()) {
        header("Location: Login.php");
    }
